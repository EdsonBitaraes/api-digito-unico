package br.com.inter.digitoUnico.controllers;

import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.services.UsuarioService;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioService usuarioService;

    private static final String URL_USUARIO = "/api/usuario";

    @Before
    public void setUp(){
        BDDMockito.given(this.usuarioService.salvar(Mockito.any(Usuario.class))).willReturn(java.util.Optional.of(new Usuario()));
        BDDMockito.given(this.usuarioService.buscarPorId(Mockito.anyLong())).willReturn(java.util.Optional.of(new Usuario()));
    }

    @Test
    public void testSalvarUsuario() throws Exception {

        JSONObject json = new JSONObject();
        json.put("nome", "Edson Pereira Bitarães Junior");
        json.put("email", "jrbitaraes@hotmail.com");

        mvc.perform(MockMvcRequestBuilders.post(URL_USUARIO)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeletarUsuario() throws Exception {
        Long idUsuario = 1L;
        mvc.perform(MockMvcRequestBuilders.delete(URL_USUARIO + "/" + idUsuario).
                accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testBuscarUsuarioPorId() throws Exception {
        Long idUsuario = 1L;
        mvc.perform(MockMvcRequestBuilders.get(URL_USUARIO + "/" + idUsuario).
                accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
