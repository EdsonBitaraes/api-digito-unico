package br.com.inter.digitoUnico.repositories;

import br.com.inter.digitoUnico.entities.DigitoUnico;
import br.com.inter.digitoUnico.entities.Usuario;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DigitoUnicoRepositoryTest {

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Before
    public void setUp(){
        Usuario usuario = new Usuario();
        usuario.setNome("Usuário teste");
        usuario.setEmail("teste@mail.com");
        usuarioRepository.save(usuario);

        List<Usuario> usuarios = usuarioRepository.findAll();
        DigitoUnico digitoUnicoSemUsuario = new DigitoUnico();
        digitoUnicoSemUsuario.setNumero(9875);
        digitoUnicoSemUsuario.setMultiplicador(4);
        digitoUnicoSemUsuario.setUsuario(usuarios.get(0));
        digitoUnicoRepository.save(digitoUnicoSemUsuario);
    }

    @After
    public final void tearDown(){
        this.digitoUnicoRepository.deleteAll();
    }

    @Test
    public void testSalvarDigitoUnico(){
        DigitoUnico digitoUnico = new DigitoUnico();
        digitoUnico.setNumero(911);
        digitoUnico.setMultiplicador(2);
        digitoUnico.setResultado("4");
        digitoUnico.setUsuario(usuarioRepository.findOne(1L));
        DigitoUnico digitoUnicoSalvo = digitoUnicoRepository.save(digitoUnico);

        DigitoUnico digitoUnicoPesquisado = digitoUnicoRepository.findOne(digitoUnico.getId());

        TestCase.assertEquals("Verifica se o dígito foi persistido com sucesso.", digitoUnicoSalvo.getId(), digitoUnicoPesquisado.getId());
    }

    @Test
    public void testBuscarDigitoUnicoPorId(){
        List<DigitoUnico> digitoUnicos =  this.digitoUnicoRepository.findAll();
        Optional<DigitoUnico> digitoUnico =  Optional.ofNullable(this.digitoUnicoRepository.findOne(digitoUnicos.get(0).getId()));

        TestCase.assertTrue("Verifica se o dígito foi recuperado com sucesso.", digitoUnico.isPresent());
    }



}
