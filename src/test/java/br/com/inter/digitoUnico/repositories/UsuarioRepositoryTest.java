package br.com.inter.digitoUnico.repositories;

import br.com.inter.digitoUnico.entities.Usuario;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Before
    public void setUp(){
        Usuario usuario = new Usuario();
        usuario.setNome("Usuário teste");
        usuario.setEmail("teste@mail.com");
        usuarioRepository.save(usuario);
    }

    @After
    public final void tearDown(){
        this.usuarioRepository.deleteAll();
    }

    @Test
    public void testSalvarUsuario(){
        Usuario usuario = new Usuario();
        usuario.setNome("Teste Inclusão");
        usuario.setEmail("teste@mail.com");
        Usuario usuarioSalvo = usuarioRepository.save(usuario);
        Usuario usuarioRecuperado = usuarioRepository.findOne(usuario.getId());

        TestCase.assertEquals("Verificar se o usário foi recuperado corretamente.", usuarioSalvo.getId(), usuarioRecuperado.getId());
    }

    @Test
    public void testBuscarUsuarioPorId(){
        List<Usuario> usuarioList =  this.usuarioRepository.findAll();
        Optional<Usuario> usuario =  Optional.ofNullable(this.usuarioRepository.findOne(usuarioList.get(0).getId()));

        TestCase.assertTrue("Verifica se o usuário foi recuperado com sucesso.", usuario.isPresent());
    }

    @Test
    public void testDeletarUsuario(){
        List<Usuario> usuarioList =  this.usuarioRepository.findAll();
        int totalUsuariosAntesDelete = usuarioList.size();

        usuarioRepository.delete(usuarioList.get(0));

        usuarioList =  this.usuarioRepository.findAll();
        int totalUsuariosDepoisDelete = usuarioList.size();

        TestCase.assertTrue("Verifica se um registro foi deletado da lista.", totalUsuariosAntesDelete > totalUsuariosDepoisDelete);
    }


}
