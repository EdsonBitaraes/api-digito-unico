package br.com.inter.digitoUnico.utils;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyPair;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CriptografiaUtilTest {

    @Test
    public void testCrptografarParametros(){
        try {
            KeyPair keyPair = CriptografiaUtil.geraChave();
            String nome = "Edson";
            String email = "jrbitaraes@hotmail.com";

            byte[] nomeEncript = CriptografiaUtil.criptografa(nome, keyPair.getPublic());
            byte[] emailEncript = CriptografiaUtil.criptografa(email, keyPair.getPublic());

            String nomeDecpt = CriptografiaUtil.decriptografa(nomeEncript, keyPair.getPrivate());
            String emailDecpt = CriptografiaUtil.decriptografa(emailEncript, keyPair.getPrivate());

            TestCase.assertTrue("Verifica de o paramtro foi criptografado e descriptografado.", nome.equalsIgnoreCase(nomeDecpt) && email.equalsIgnoreCase(emailDecpt));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
