package br.com.inter.digitoUnico.controllers;

import br.com.inter.digitoUnico.dtos.DigitoUnicoDto;
import br.com.inter.digitoUnico.dtos.UsuarioDto;
import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.response.Response;
import br.com.inter.digitoUnico.services.UsuarioService;
import br.com.inter.digitoUnico.utils.ConvesaoEntidadesUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "*")
public class UsuarioController {

    private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);

    @Autowired
    private UsuarioService usuarioService;

    /**
     * Cadastrar um usuário no banco de dados
     *
     * @param usuarioDto
     * @return ResponseEntity<Response   <   UsuarioDto>>
     */
    @ApiOperation(
            value = "Salvar",
            response = DigitoUnicoDto.class,
            notes = "Está operação salva um usuário no banco")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200,
                    message = "Retorna um Response com um usuarioDto",
                    response = Response.class
            ),
            @ApiResponse(
                    code = 500,
                    message = "Caso tenhamos algum erro retorna um Response uma lista de erros Exception",
                    response = Response.class
            )

    })
    @PostMapping
    public ResponseEntity<Response<UsuarioDto>> salvar(@Valid @RequestBody UsuarioDto usuarioDto) {
        log.info("Cadastrando usuário no banco de dados.");
        Optional<Usuario> usuario = usuarioService.salvar(ConvesaoEntidadesUtil.convertDtoForUsuario(usuarioDto));

        Response<UsuarioDto> response = new Response<>();
        response.setData(ConvesaoEntidadesUtil.convertUsuarioForDto(usuario.get()));

        return ResponseEntity.ok(response);
    }

    /**
     * Deletar um usuário no banco de dados
     *
     * @param usuarioDto
     * @return ResponseEntity<Response < UsuarioDto>>
     */
    @ApiOperation(
            value = "Deletar",
            response = DigitoUnicoDto.class,
            notes = "Está operação deleta um usuário no banco")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200,
                    message = "Retorna um Response com um usuarioDto",
                    response = Response.class
            ),
            @ApiResponse(
                    code = 500,
                    message = "Caso tenhamos algum erro retorna um Response uma lista de erros Exception",
                    response = Response.class
            )

    })
    @DeleteMapping
    public ResponseEntity<Response<UsuarioDto>> deletar(@RequestBody UsuarioDto usuarioDto) {
        log.info("Deletando usuário no banco de dados {}", usuarioDto);
        Response<UsuarioDto> response = new Response<>();

        if (usuarioDto.getId() == null) {
            log.info("Erro ao deletar usuario {}", usuarioDto);
            response.getErrors().add(new ObjectError("usuario", "Id do usuário é obrigatorio para deletar.").getDefaultMessage());
            return ResponseEntity.badRequest().body(response);
        }

        try {
            usuarioService.deletar(usuarioDto.getId());
            response.setData(usuarioDto);
            return ResponseEntity.ok(response);
        } catch (EmptyResultDataAccessException ex) {
            response.getErrors().add(new ObjectError("usuario", "Não existe nenhum registro com o id " + usuarioDto.getId()).getDefaultMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    /**
     * Buscar um usuário no banco de dados por id.
     *
     * @param id
     * @return ResponseEntity<Response < UsuarioDto>>
     */
    @ApiOperation(
            value = "buscarUsuarioPorId",
            response = DigitoUnicoDto.class,
            notes = "Está operação obtem um usuário por id")
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200,
                    message = "Retorna um Response com um UsuarioDto",
                    response = Response.class
            ),
            @ApiResponse(
                    code = 500,
                    message = "Caso tenhamos algum erro retorna um Response uma lista de erros Exception",
                    response = Response.class
            )

    })
    @RequestMapping(value = "/{id}")
    public ResponseEntity<Response<UsuarioDto>> buscarUsuarioPorId(@PathVariable("id") Long id) {
        log.info("Buscando usuário por id {}", id);
        Response<UsuarioDto> response = new Response<>();

        Optional<Usuario> usuario = usuarioService.buscarPorId(id);

        if(!usuario.isPresent()){
            log.info("Usuário não encontrado para o id {}", id);
            response.getErrors().add(new ObjectError("usuario", "Não foi encontrado nenhum usuário para o id " + id).getDefaultMessage());
            return ResponseEntity.badRequest().body(response);
        }

        response.setData(ConvesaoEntidadesUtil.convertUsuarioForDto(usuario.get()));
        return ResponseEntity.ok(response);
    }
}
