package br.com.inter.digitoUnico.controllers;

import br.com.inter.digitoUnico.dtos.DigitoUnicoDto;
import br.com.inter.digitoUnico.entities.DigitoUnico;
import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.exeptions.UsuarioNaoEncontradoExeption;
import br.com.inter.digitoUnico.response.Response;
import br.com.inter.digitoUnico.services.DigitoUnicoService;
import br.com.inter.digitoUnico.services.UsuarioService;
import br.com.inter.digitoUnico.utils.ConvesaoEntidadesUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/digito-unico")
@CrossOrigin(origins="*")
public class DigitoUnicoController {

    private static final Logger log = LoggerFactory.getLogger(DigitoUnicoController.class);

    @Autowired
    private DigitoUnicoService digitoUnicoService;

    @Autowired
    private UsuarioService usuarioService;

    /**
     * Calcular o dígito único.
     *
     * @Param digitoUnicoDto
     * @Return ResponseEntity<Response<DigitoUnicoDto>>
     */
    @ApiOperation(
            value="Cadastrar uma nova pessoa",
            response=DigitoUnicoDto.class,
            notes="Essa operação calcula o dígito único.")
    @ApiResponses(value= {
            @ApiResponse(
                    code=200,
                    message="Retorna um Response com um DigitoUnicoDto",
                    response=Response.class
            ),
            @ApiResponse(
                    code=500,
                    message="Caso tenhamos algum erro retorna um Response uma lista de erros Exception",
                    response=Response.class
            )

    })
    @PostMapping
    public ResponseEntity<Response<DigitoUnicoDto>> calcular(@Valid @RequestBody DigitoUnicoDto digitoUnicoDto) {
        Response<DigitoUnicoDto> response = new Response<>();

        DigitoUnico digitoUnico = digitoUnicoService.calcular(digitoUnicoDto.getNumero(), digitoUnicoDto.getMultiplicador());

        try {
            digitoUnico.setUsuario(new Usuario(digitoUnicoDto.getIdUsuario()));
            gravarDigitoUnicoCasoVinculadoAoUsuario(digitoUnico);
        } catch (UsuarioNaoEncontradoExeption usuarioNaoEncontradoExeption) {
            response.getErrors().add(usuarioNaoEncontradoExeption.getMessage());
        }

        response.setData(ConvesaoEntidadesUtil.converterDigitoUnicoForDto(digitoUnico));
        return ResponseEntity.ok(response);
    }

    /**
     * Vincular o dígito único a um usuário.
     *
     * @param digitoUnico
     * @return List<String>
     * @throws UsuarioNaoEncontradoExeption
     * */
    private void gravarDigitoUnicoCasoVinculadoAoUsuario(DigitoUnico digitoUnico) throws UsuarioNaoEncontradoExeption {
        verificaUsuarioValido(digitoUnico.getUsuario().getId());

        if(digitoUnico.getUsuario().getId() != null){
            log.info("Persistindo digito único vinculado ao usuário: {}", digitoUnico);
            digitoUnicoService.salvar(digitoUnico);
        }
    }

    /**
     * Verifica se existe um usuário válido para o id informado.
     *
     * @param idUsuario
     * @throws UsuarioNaoEncontradoExeption
     * */
    private void verificaUsuarioValido(Long idUsuario) throws UsuarioNaoEncontradoExeption {
        log.info("Verificando se o usuário é válido: {}", idUsuario);
        Optional<Usuario> usuario = usuarioService.buscarPorId(idUsuario);
        if(!usuario.isPresent()){
            throw new UsuarioNaoEncontradoExeption("Não foi encontrado nenhum usuário para o id " + idUsuario);
        }
    }
}
