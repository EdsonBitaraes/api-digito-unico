package br.com.inter.digitoUnico.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="digitoUnico")
@Getter @Setter @NoArgsConstructor
public class DigitoUnico implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long id;

    @Column
    public Integer numero;

    @Column
    public Integer multiplicador;

    @Column
    public String resultado;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    public Usuario usuario;

    public DigitoUnico(Integer numero, Integer multiplicador, String resultado) {
        this.numero = numero;
        this.multiplicador = multiplicador;
        this.resultado = resultado;
    }

    public Usuario getUsuario() {
        return usuario == null ? new Usuario() : usuario;
    }
}
