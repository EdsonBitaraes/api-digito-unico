package br.com.inter.digitoUnico.utils;

import br.com.inter.digitoUnico.exeptions.CriptografiaExeption;

import javax.crypto.Cipher;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;

public class CriptografiaUtil {

    public static final String ALGORITHM = "RSA";

    /**
     * Gera a chave que contém um par de chave Privada e Pública usando 2048 bytes.
     *
     * @Return KeyPair
     * @throws CriptografiaExeption
     */
    public static KeyPair geraChave() throws CriptografiaExeption {
        KeyPairGenerator keyGen = null;
        try {
            keyGen = KeyPairGenerator.getInstance(ALGORITHM);
            keyGen.initialize(2048);
        } catch (Exception e) {
            throw new CriptografiaExeption("Erro ao gerar chave pública e privada.", e);
        }

        return keyGen.generateKeyPair();
    }

    /**
     * Criptografa o texto puro usando chave pública.
     *
     * @Param texto
     * @Param chave
     * @Return byte[]
     * @throws CriptografiaExeption
     */
    public static byte[] criptografa(String texto, PublicKey chave) throws CriptografiaExeption {
        byte[] cipherText = null;

        try {
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, chave);
            cipherText = cipher.doFinal(texto.getBytes());
        } catch (Exception e) {
            throw new CriptografiaExeption("Erro ao gerar chave pública e privada.", e);
        }

        return cipherText;
    }

    /**
     * Decriptografa o texto puro usando chave privada.
     *
     * @Param texto
     * @Param chave
     * @Return String
     * @throws CriptografiaExeption
     */
    public static String decriptografa(byte[] texto, PrivateKey chave) throws CriptografiaExeption {
        byte[] dectyptedText = null;

        try {
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, chave);
            dectyptedText = cipher.doFinal(texto);
        } catch (Exception e) {
            throw new CriptografiaExeption("Erro ao gerar chave pública e privada.", e);
        }

        return new String(dectyptedText);
    }

    /**
     * Recuperar um uma private key a partir de byte[].
     *
     * @Param encodedPrivateKey
     * @Param PrivateKey
     * @throws CriptografiaExeption
     */
    public static PrivateKey getPrivateKey(byte[] encodedPrivateKey) throws CriptografiaExeption {
        try {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
            KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
            PrivateKey priKey = keyFactory.generatePrivate(keySpec);
            return priKey;
        } catch (Exception e) {
            throw new CriptografiaExeption("Erro ao recuperar a chave privada.", e);
        }
    }
}
