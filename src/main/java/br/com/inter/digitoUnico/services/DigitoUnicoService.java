package br.com.inter.digitoUnico.services;

import br.com.inter.digitoUnico.entities.DigitoUnico;

public interface DigitoUnicoService {

    /**
     * Persiste um dígito único no banco de dados
     *
     * @param digitoUnico
     * @return DigitoUnico
     * */
    DigitoUnico salvar(DigitoUnico digitoUnico);

    /**
    * Calcula o dígito único e vincula á um usuário
    *
    * @param numero
    * @param multiplo
    * @return DigitoUnico
    * */
    DigitoUnico calcular(Integer numero, Integer multiplo);
}
