package br.com.inter.digitoUnico.services.impl;

import br.com.inter.digitoUnico.entities.Usuario;
import br.com.inter.digitoUnico.repositories.UsuarioRepository;
import br.com.inter.digitoUnico.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);

    @Autowired
    private UsuarioRepository usuarioRepository;

    /*
     * Método responsável por buscar um usúario a partir de um id.
     *
     * @param id
     * @return Optional<Usuario>
     */
    @Override
    public Optional<Usuario> buscarPorId(Long id)  {
        return Optional.ofNullable(usuarioRepository.findOne(id));
    }

    /*
     * Método responsável por persistir o usuário no banco.
     *
     * @param usuario
     * @return Usuario
     */
    @Override
    public Optional<Usuario> salvar(Usuario usuario) {
        log.info("Persisitindo usuario {}", usuario);
        return Optional.ofNullable(usuarioRepository.save(usuario));
    }

    /*
     * Método responsável por deletar um usuáro no banco..
     *
     * @param id
     * @return void
     */
    @Override
    public Optional<Usuario> deletar(Long id) {
        usuarioRepository.delete(id);
        return Optional.ofNullable(new Usuario(id));
    }
}
