package br.com.inter.digitoUnico.repositories;

import br.com.inter.digitoUnico.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
}
