package br.com.inter.digitoUnico.exeptions;

public class UsuarioNaoEncontradoExeption extends Exception {
    public UsuarioNaoEncontradoExeption(String errorMessage) {
        super(errorMessage);
    }
}
