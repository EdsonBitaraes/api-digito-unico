package br.com.inter.digitoUnico.exeptions;

public class CriptografiaExeption extends Exception{

    public CriptografiaExeption(String message, Throwable cause) {
        super(message, cause);
    }
}
