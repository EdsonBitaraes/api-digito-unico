# api-digito-unico

API para cálculo do dígito único. Aplicação construida em spring boot 1.5.3.RELEASE e java 8.

1 - Para compilar a aplicação via maven, utiliar o comando:

mvn spring-boot:run

2 - Para rodar os testes unitários, utilizar o comando:

mvn test

Endpoints da API:

Crud Usuário:
Salvar e alterar: @post http://localhost:8080/api/usuario
Deletar: @delete http://localhost:8080/api/usuario
Recuperar e listar os digitos únicos por usuário: @get http://localhost:8080/api/usuario/1

Dígito único
Calcular dígito único - @post http://localhost:8080/api/digito-unico


**Debito técnico: A aplicação é capaz de gerar as chaves publicas e privadas e fazer o fluxo de criptografia através da 
classe CriptografiaUtil, como mostra o teste CriptografiaUtilTest, mas ao tentar uma recuperar uma chave privada do banco, 
o sistema retorna o erro "Data must not be longer than 256 bytes",com isso a camada de criptografia não foi implementada 
nos controllers da API.